var deployd = require("deployd"),
	ip = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1",
	port = process.env.OPENSHIFT_NODEJS_PORT || 8080,
	dbUrl = process.env.OPENSHIFT_MONGODB_DB_URL || "mongodb://localhost:3456/deployd",
	dpd = deployd({
		port: port,
		env: process.env.NODE_ENV || "development",
		db: {
			connectionString: dbUrl
		}
	});

dpd.listen(port, ip);

