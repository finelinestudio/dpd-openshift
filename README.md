# OpenShift project template for deployd

Can be used for creating a new auto-scaling OpenShift deployd application as follows:
 
	rhc app-create <app-name> nodejs-0.10 mongodb-2.4 -s [-l <openshift-username>] --from-code <this-repo-url>

The project will be git-cloned into a local directory with the app's name. 
This can be run locally by first starting mongo. Within the app dir:

	mongod --dbpath ./data --port 3456 -f /dev/null --nojournal --smallfiles --nssize 4
	
Then run:

	node start
	
The project could be linked to a separate source control repo as its default master,
by first renaming the OpenShift repo as follows:

	git remote rename origin openshift
	git add remote origin <githuburl>

Regular development would then use normal git pushes.

	git push
 
Production deployments would use the OpenShift named remote:

	git push openshift
	
