var gulp = require("gulp"),
	fs = require("fs"),
	exec = require('child_process').exec;

gulp.task("mongod", function() {
	if (!fs.existsSync("./data")) fs.mkdir("./data");
	exec("mongod --dbpath ./data --port 3456 -f /dev/null --nojournal --smallfiles --nssize 4");
});

gulp.task("server", function() {
	require("./start");
});